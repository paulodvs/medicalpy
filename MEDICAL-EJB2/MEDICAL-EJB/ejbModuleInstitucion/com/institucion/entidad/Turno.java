package com.institucion.entidad;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "turno")
public class Turno implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "tur_nro")
	private Integer tur_nro;
	
	@Column(name = "tur_desc")
	private String tur_desc;

	public Turno() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getTur_nro() {
		return tur_nro;
	}

	public void setTur_nro(Integer tur_nro) {
		this.tur_nro = tur_nro;
	}

	public String getTur_desc() {
		return tur_desc;
	}

	public void setTur_desc(String tur_desc) {
		this.tur_desc = tur_desc;
	}
	
}
