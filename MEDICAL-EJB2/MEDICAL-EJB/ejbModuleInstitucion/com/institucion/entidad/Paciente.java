package com.institucion.entidad;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.general.entidad.Persona;

@Entity
@Table(name = "paciente")
public class Paciente implements Serializable{

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pac_nro")
	private Integer pac_nro;
	@Column(name = "per_nro")
	private ArrayList<Persona> per_nro;
	@Column(name = "pac_desc")
	private String pac_desc;
	@Column(name = "pac_fch")
	@Temporal(TemporalType.TIMESTAMP)
	private Date pac_fch;
	
	@Column(name = "hos_nro")
	private ArrayList<Hospital> hos_nro;
	
	@Column(name = "tur_nro")
	private ArrayList<Turno> tur_nro;

	public Paciente() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getPac_nro() {
		return pac_nro;
	}

	public void setPac_nro(Integer pac_nro) {
		this.pac_nro = pac_nro;
	}

	public ArrayList<Persona> getPer_nro() {
		return per_nro;
	}

	public void setPer_nro(ArrayList<Persona> per_nro) {
		this.per_nro = per_nro;
	}

	public String getPac_desc() {
		return pac_desc;
	}

	public void setPac_desc(String pac_desc) {
		this.pac_desc = pac_desc;
	}

	public Date getPac_fch() {
		return pac_fch;
	}

	public void setPac_fch(Date pac_fch) {
		this.pac_fch = pac_fch;
	}

	public ArrayList<Hospital> getHos_nro() {
		return hos_nro;
	}

	public void setHos_nro(ArrayList<Hospital> hos_nro) {
		this.hos_nro = hos_nro;
	}

	public ArrayList<Turno> getTur_nro() {
		return tur_nro;
	}

	public void setTur_nro(ArrayList<Turno> tur_nro) {
		this.tur_nro = tur_nro;
	}

}
