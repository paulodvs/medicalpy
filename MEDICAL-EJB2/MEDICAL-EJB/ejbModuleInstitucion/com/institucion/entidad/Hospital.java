package com.institucion.entidad;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "hospital")
public class Hospital implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "hos_nro")
	private Integer hos_nro;
	
	@Column(name = "  hos_desc")
	private String hos_desc;

	public Hospital() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getHos_nro() {
		return hos_nro;
	}

	public void setHos_nro(Integer hos_nro) {
		this.hos_nro = hos_nro;
	}

	public String getHos_desc() {
		return hos_desc;
	}

	public void setHos_desc(String hos_desc) {
		this.hos_desc = hos_desc;
	}
	
}
