package com.institucion.entidad;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "translado")
public class Translado implements Serializable{

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "trans_nro")
	private Integer trans_nro;
	
	@Column(name = "tans_desc")
	private String trans_desc;
	
	@Column(name = "trans_fch")
	private Date trans_fch;

	public Translado() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getTrans_nro() {
		return trans_nro;
	}

	public void setTrans_nro(Integer trans_nro) {
		this.trans_nro = trans_nro;
	}

	public String getTrans_desc() {
		return trans_desc;
	}

	public void setTrans_desc(String trans_desc) {
		this.trans_desc = trans_desc;
	}

	public Date getTrans_fch() {
		return trans_fch;
	}

	public void setTrans_fch(Date trans_fch) {
		this.trans_fch = trans_fch;
	}
	
	
}
