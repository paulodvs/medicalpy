package com.institucion.entidad;

import java.io.Serializable;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.general.entidad.Persona;

@Entity
@Table(name = "especialista")
public class Especialista implements Serializable{


	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "esp_nro")
	private Integer esp_nro;
	@Column(name = "tur_nro")
	private ArrayList<Turno> tunoNro;
	@Column(name = "esp_antiguedad")
	private Integer esp_antiguedad;
	@Column(name = "per_nro")
	private ArrayList<Persona> per_nro;
	public Especialista() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Integer getEsp_nro() {
		return esp_nro;
	}
	public void setEsp_nro(Integer esp_nro) {
		this.esp_nro = esp_nro;
	}
	public ArrayList<Turno> getTunoNro() {
		return tunoNro;
	}
	public void setTunoNro(ArrayList<Turno> tunoNro) {
		this.tunoNro = tunoNro;
	}
	public Integer getEsp_antiguedad() {
		return esp_antiguedad;
	}
	public void setEsp_antiguedad(Integer esp_antiguedad) {
		this.esp_antiguedad = esp_antiguedad;
	}
	public ArrayList<Persona> getPer_nro() {
		return per_nro;
	}
	public void setPer_nro(ArrayList<Persona> per_nro) {
		this.per_nro = per_nro;
	}
	
}
