package com.institucion.entidad;
import java.io.Serializable;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.general.entidad.Semestres;
import com.general.entidad.Zona;
import com.medicina.entidad.Casos;
import com.medicina.entidad.Diagnostico;
import com.medicina.entidad.Recetas;
import com.medicina.entidad.Sintomas;

@Entity
@Table(name = "expediente")
public class Expediente implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy =GenerationType.IDENTITY )
	@Column(name = "exp_nro")
	private Integer exp_nro;
	
	@Column(name = "caso_nro")
	private ArrayList<Casos>caso_nro;
	
	@Column(name = "diag_nro")
	private ArrayList<Diagnostico>diagnostico_nro;
	
	@Column(name = "exp_fch")
	private Expediente exp_fecha;
	
	@Column(name = "trans_nro")
	private ArrayList<Translado> trans_nro;
	
	@Column(name = "exp_edad_riesgo")
	private ArrayList<Expediente> exp_edad_riesgo;
	
	@Column(name = "exp_result_entrega")
	private Expediente exp_result_entrega;
	
	@Column(name = "observaciones")
	private String obs;
	
	@Column(name = "pac_nro")
	private ArrayList<Paciente> pac_nro;
	
	@Column(name = "casos_nro")
	private Casos casos_nro;
	
	@Column(name = "rec_nro")
	private ArrayList<Recetas> rec_nro;
	
	@Column(name = "sin_nro")
	private Sintomas sin_nro;
	
	@Column(name = "zo_nro")
	private Zona zo_nro;
	
	@Column(name = "esp_nro")
	private Semestres sem_nro;

	public Expediente() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getExp_nro() {
		return exp_nro;
	}

	public void setExp_nro(Integer exp_nro) {
		this.exp_nro = exp_nro;
	}

	public ArrayList<Casos> getCaso_nro() {
		return caso_nro;
	}

	public void setCaso_nro(ArrayList<Casos> caso_nro) {
		this.caso_nro = caso_nro;
	}

	public ArrayList<Diagnostico> getDiagnostico_nro() {
		return diagnostico_nro;
	}

	public void setDiagnostico_nro(ArrayList<Diagnostico> diagnostico_nro) {
		this.diagnostico_nro = diagnostico_nro;
	}

	public Expediente getExp_fecha() {
		return exp_fecha;
	}

	public void setExp_fecha(Expediente exp_fecha) {
		this.exp_fecha = exp_fecha;
	}

	public ArrayList<Translado> getTrans_nro() {
		return trans_nro;
	}

	public void setTrans_nro(ArrayList<Translado> trans_nro) {
		this.trans_nro = trans_nro;
	}

	public ArrayList<Expediente> getExp_edad_riesgo() {
		return exp_edad_riesgo;
	}

	public void setExp_edad_riesgo(ArrayList<Expediente> exp_edad_riesgo) {
		this.exp_edad_riesgo = exp_edad_riesgo;
	}

	public Expediente getExp_result_entrega() {
		return exp_result_entrega;
	}

	public void setExp_result_entrega(Expediente exp_result_entrega) {
		this.exp_result_entrega = exp_result_entrega;
	}

	public String getObs() {
		return obs;
	}

	public void setObs(String obs) {
		this.obs = obs;
	}

	public ArrayList<Paciente> getPac_nro() {
		return pac_nro;
	}

	public void setPac_nro(ArrayList<Paciente> pac_nro) {
		this.pac_nro = pac_nro;
	}

	public Casos getCasos_nro() {
		return casos_nro;
	}

	public void setCasos_nro(Casos casos_nro) {
		this.casos_nro = casos_nro;
	}

	public ArrayList<Recetas> getRec_nro() {
		return rec_nro;
	}

	public void setRec_nro(ArrayList<Recetas> rec_nro) {
		this.rec_nro = rec_nro;
	}

	public Sintomas getSin_nro() {
		return sin_nro;
	}

	public void setSin_nro(Sintomas sin_nro) {
		this.sin_nro = sin_nro;
	}

	public Zona getZo_nro() {
		return zo_nro;
	}

	public void setZo_nro(Zona zo_nro) {
		this.zo_nro = zo_nro;
	}

	public Semestres getSem_nro() {
		return sem_nro;
	}

	public void setSem_nro(Semestres sem_nro) {
		this.sem_nro = sem_nro;
	}
	
	
	
}
