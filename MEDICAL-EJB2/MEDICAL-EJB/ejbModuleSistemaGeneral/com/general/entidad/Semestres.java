package com.general.entidad;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "semestres")
public class Semestres implements Serializable{

	
	private static final long serialVersionUID = 1L;

	/***
	 * @author PauloVillamayor-Inge
	 * @param se carga el sementre de cada acontesimiento
	 * @category zona implementacion 
	 * 
	 */
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sem_nro")
	private Integer codigo;
	@Column( name = "sem_desc")
	
	
	private String nombre;
	
	public Semestres() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	

}
