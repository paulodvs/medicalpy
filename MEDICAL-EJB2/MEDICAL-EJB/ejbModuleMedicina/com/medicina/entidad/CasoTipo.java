package com.medicina.entidad;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "casos_tipo")
public class CasoTipo implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "casos_tipo_nro")
	private Integer caso_tipo_nro;
	
	@Column(name = "casos_tipo_desc")
	private String caso_tipo_desc;

	public CasoTipo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getCaso_tipo_nro() {
		return caso_tipo_nro;
	}

	public void setCaso_tipo_nro(Integer caso_tipo_nro) {
		this.caso_tipo_nro = caso_tipo_nro;
	}

	public String getCaso_tipo_desc() {
		return caso_tipo_desc;
	}

	public void setCaso_tipo_desc(String caso_tipo_desc) {
		this.caso_tipo_desc = caso_tipo_desc;
	}
	
}
