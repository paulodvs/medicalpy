package com.medicina.entidad;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "receta_tipos")
public class TipoRecetas implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "rec_tipo_nro")
	private Integer recetas_tipo_codigo;
	
	@Column(name = "rec_tipo_desc")
	private String recetas_tipo_descripcion;

	public TipoRecetas() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getRecetas_tipo_codigo() {
		return recetas_tipo_codigo;
	}

	public void setRecetas_tipo_codigo(Integer recetas_tipo_codigo) {
		this.recetas_tipo_codigo = recetas_tipo_codigo;
	}

	public String getRecetas_tipo_descripcion() {
		return recetas_tipo_descripcion;
	}

	public void setRecetas_tipo_descripcion(String recetas_tipo_descripcion) {
		this.recetas_tipo_descripcion = recetas_tipo_descripcion;
	}
	

	
	
}
