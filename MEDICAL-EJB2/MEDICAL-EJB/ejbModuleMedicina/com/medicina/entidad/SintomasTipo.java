package com.medicina.entidad;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "sintomas_tipos")
public class SintomasTipo implements Serializable{

	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sin_tipo_nro")
	private Integer sintomas_tipo_numero;
	
	@Column(name = "sin_tipo_desc")
	private String sintomas_tipo_descripcion;

	public SintomasTipo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getSintomas_tipo_numero() {
		return sintomas_tipo_numero;
	}

	public void setSintomas_tipo_numero(Integer sintomas_tipo_numero) {
		this.sintomas_tipo_numero = sintomas_tipo_numero;
	}

	public String getSintomas_tipo_descripcion() {
		return sintomas_tipo_descripcion;
	}

	public void setSintomas_tipo_descripcion(String sintomas_tipo_descripcion) {
		this.sintomas_tipo_descripcion = sintomas_tipo_descripcion;
	}
	

}
