package com.medicina.entidad;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "casos")
public class Casos implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "caso_nro")
	private Integer caso_numero;
	
	@Column(name = "caso_desc")
	private String caso_desc;
	
	@Column(name = "casos_tipo_nro")
	private CasoTipo caso_tipo_nro;

	public Casos() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getCaso_numero() {
		return caso_numero;
	}

	public void setCaso_numero(Integer caso_numero) {
		this.caso_numero = caso_numero;
	}

	public String getCaso_desc() {
		return caso_desc;
	}

	public void setCaso_desc(String caso_desc) {
		this.caso_desc = caso_desc;
	}

	public CasoTipo getCaso_tipo_nro() {
		return caso_tipo_nro;
	}

	public void setCaso_tipo_nro(CasoTipo caso_tipo_nro) {
		this.caso_tipo_nro = caso_tipo_nro;
	}
	
	
}
