package com.medicina.entidad;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "sintomas")
public class Sintomas  implements Serializable{


	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sin_nro")
	private Integer sintomas_numero;
	@Column(name = "sin_nro_desc")
	private String sintomas_descripcion;
	@Column(name = "sin_tipo_nro")
	private SintomasTipo sintomasTipo;

	public Sintomas() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Integer getSintomas_numero() {
		return sintomas_numero;
	}
	public void setSintomas_numero(Integer sintomas_numero) {
		this.sintomas_numero = sintomas_numero;
	}
	public String getSintomas_descripcion() {
		return sintomas_descripcion;
	}
	public void setSintomas_descripcion(String sintomas_descripcion) {
		this.sintomas_descripcion = sintomas_descripcion;
	}
	public SintomasTipo getSintomasTipo() {
		return sintomasTipo;
	}
	public void setSintomasTipo(SintomasTipo sintomasTipo) {
		this.sintomasTipo = sintomasTipo;
	}
	
}
