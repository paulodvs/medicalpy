package com.medicina.entidad;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "diagnostico_tipo")
public class DiagnosticoTIpo  implements Serializable{

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "diag_nro_tipo")
	private Integer dianostico_numero_tipo;
	
	@Column(name = "diag_desc")
	private String dianostico_desc;

	public DiagnosticoTIpo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getDianostico_numero_tipo() {
		return dianostico_numero_tipo;
	}

	public void setDianostico_numero_tipo(Integer dianostico_numero_tipo) {
		this.dianostico_numero_tipo = dianostico_numero_tipo;
	}

	public String getDianostico_desc() {
		return dianostico_desc;
	}

	public void setDianostico_desc(String dianostico_desc) {
		this.dianostico_desc = dianostico_desc;
	}
	
	
}
