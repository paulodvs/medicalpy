package com.medicina.entidad;

import java.io.Serializable;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "diagnostico")
public class Diagnostico implements Serializable{
	
	
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "rec_nro")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer diagnostico_numero;
	
	@Column(name = "rec_desc")
	private String diagnostico_descripcion;
	
	@Column(name = "rec_tipo_nro")
	ArrayList<DiagnosticoTIpo> diagnostico;
	private TipoRecetas tiporecetas;
	
	
	public Diagnostico() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Integer getDiagnostico_numero() {
		return diagnostico_numero;
	}


	public void setDiagnostico_numero(Integer diagnostico_numero) {
		this.diagnostico_numero = diagnostico_numero;
	}


	public String getDiagnostico_descripcion() {
		return diagnostico_descripcion;
	}


	public void setDiagnostico_descripcion(String diagnostico_descripcion) {
		this.diagnostico_descripcion = diagnostico_descripcion;
	}


	public ArrayList<DiagnosticoTIpo> getDiagnostico() {
		return diagnostico;
	}


	public void setDiagnostico(ArrayList<DiagnosticoTIpo> diagnostico) {
		this.diagnostico = diagnostico;
	}


	public TipoRecetas getTiporecetas() {
		return tiporecetas;
	}


	public void setTiporecetas(TipoRecetas tiporecetas) {
		this.tiporecetas = tiporecetas;
	}
	
	
	
	
	

}
