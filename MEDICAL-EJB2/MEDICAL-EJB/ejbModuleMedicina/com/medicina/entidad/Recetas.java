package com.medicina.entidad;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;



@Entity
@Table(name = "recetas")
public class Recetas  implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "rec_nro")
	private Integer codigo;
			
	@Column(name = "rec_desc")
	private String recetas_descripcion;
	
	@Column(name = "rec_fch")
	@Temporal(TemporalType.TIMESTAMP)
	private Date recetas_fechas;
	
	
	@Column(name = "rec_tipo_nro")	
	private ArrayList<TipoRecetas> tipoRecetas;
	
	public Recetas() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getRecetas_descripcion() {
		return recetas_descripcion;
	}

	public void setRecetas_descripcion(String recetas_descripcion) {
		this.recetas_descripcion = recetas_descripcion;
	}

	public Date getRecetas_fechas() {
		return recetas_fechas;
	}

	public void setRecetas_fechas(Date recetas_fechas) {
		this.recetas_fechas = recetas_fechas;
	}

	public ArrayList<TipoRecetas> getTipoRecetas() {
		return tipoRecetas;
	}

	public void setTipoRecetas(ArrayList<TipoRecetas> tipoRecetas) {
		this.tipoRecetas = tipoRecetas;
	}


}
